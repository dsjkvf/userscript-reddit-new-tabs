reddit: open links in new tabs
==============================

## About

This is a small [userscript](https://github.com/OpenUserJs/OpenUserJS.org/wiki/Userscript-beginners-HOWTO) for [Greasemonkey](https://addons.mozilla.org/firefox/addon/greasemonkey/) or similar extension, which -- upon visiting [reddit](https://www.reddit.com/) pages -- will open title and comments links in new tabs.

// ==UserScript==
// @name        reddit - open links in new tabs
// @namespace   oyvey
// @homepage    https://bitbucket.org/dsjkvf/userscript-reddit-new-tabs
// @downloadURL https://bitbucket.org/dsjkvf/userscript-reddit-new-tabs/raw/master/rent.user.js
// @updateURL   https://bitbucket.org/dsjkvf/userscript-reddit-new-tabs/raw/master/rent.user.js
// @match       *://*.reddit.com/*
// @require     https://gist.github.com/raw/2625891/waitForKeyElements.js
// @run-at      document-end
// @grant       none
// @version     1.0.9
// ==/UserScript==

// SETTINGS

const names = ['myrisingstocks', 'ringorangorongo', 'alasdairgray', '/duplicates/'];

// HELPERS

function linksInNewTabs() {
    var links = document.getElementsByTagName('a');
    for (var i = 0; i < links.length; i++) {
        if (
            /count.*before/.test(links[i]) == false &&
            /count.*after/.test(links[i]) == false &&
            !names.some(k => links[i].href.includes(k)) &&
            links[i].href.indexOf('reddit.com/message/inbox') < 0 &&
            links[i].href.indexOf('javascript:void') < 0
        )
        {
            links[i].target = '_blank';
        };
    };
};

// MAIN

waitForKeyElements('div.md', linksInNewTabs );
